#ifndef __GAMESCENE_H__
#define __GAMESCENE_H__

#include "cocos2d.h"

#include "president/President.h"
#include "interface/SoundMenu.h"
#include "interface/PauseMenu.h"
#include "interface/ProgressInfo.h"
#include "entity/MovingEntity.h"
#include "entity/MovingEntityFactory.h"

class GameScene: public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();
private:
	cocos2d::PhysicsWorld *sceneWorld;
	President* president;
	ProgressInfo* progressInfo;
	MovingEntityFactory* movingEntityFactory;

	const Vec2* starCoord;
	bool starCreate;

	void onMouseDown(EventMouse* event);
	void onMouseUp(EventMouse* event);
	void setPhysicsWorld(cocos2d::PhysicsWorld *world);

	void tapObamaCallback(cocos2d::Ref* pSender);
	void updateTimer(float dt);
	void update(float dt);

	void removeAll();

	bool onContactBegin(PhysicsContact &contact);


	CREATE_FUNC(GameScene);
};

#endif // __GAMESCENE_H__