#include "President.h"

#include "MyBodyParser.h"
#include "CollisionBitmasks.h"

President::President()
{
	visibleSize = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();

	auto spritecache = SpriteFrameCache::getInstance();

	animFrames1 = new Vector<SpriteFrame*>();
	animFrames1->pushBack(spritecache->getSpriteFrameByName("fly/obama fly1.png"));
	animFrames1->pushBack(spritecache->getSpriteFrameByName("fly/obama fly2.png"));
	animFrames1->pushBack(spritecache->getSpriteFrameByName("fly/obama fly3.png"));
	animFrames1->pushBack(spritecache->getSpriteFrameByName("fly/obama fly4.png"));

	animFrames2 = new Vector<SpriteFrame*>();
	animFrames2->pushBack(spritecache->getSpriteFrameByName("eat/obama eat1.png"));
	animFrames2->pushBack(spritecache->getSpriteFrameByName("eat/obama eat2.png"));
	animFrames2->pushBack(spritecache->getSpriteFrameByName("eat/obama eat3.png"));
	animFrames2->pushBack(spritecache->getSpriteFrameByName("eat/obama eat4.png"));
	animFrames2->pushBack(spritecache->getSpriteFrameByName("eat/obama eat5.png"));
	animFrames2->pushBack(spritecache->getSpriteFrameByName("eat/obama eat6.png"));
	animFrames2->pushBack(spritecache->getSpriteFrameByName("eat/obama eat7.png"));
	animFrames2->pushBack(spritecache->getSpriteFrameByName("eat/obama eat8.png"));

	animFrames3 = new Vector<SpriteFrame*>();
	animFrames3->pushBack(spritecache->getSpriteFrameByName("fall2/fall_electro1.png"));
	animFrames3->pushBack(spritecache->getSpriteFrameByName("fall2/fall_electro2.png"));

	animFrames4 = new Vector<SpriteFrame*>();
	animFrames4->pushBack(spritecache->getSpriteFrameByName("fall2/fall1.png"));
	animFrames4->pushBack(spritecache->getSpriteFrameByName("fall2/fall2.png"));
	animFrames4->pushBack(spritecache->getSpriteFrameByName("fall2/fall3.png"));
	animFrames4->pushBack(spritecache->getSpriteFrameByName("fall2/fall4.png"));

	animFrames5 = new Vector<SpriteFrame*>();
	animFrames5->pushBack(spritecache->getSpriteFrameByName("fall1/fall1.png"));
	animFrames5->pushBack(spritecache->getSpriteFrameByName("fall1/fall2.png"));
	animFrames5->pushBack(spritecache->getSpriteFrameByName("fall1/fall3.png"));
	animFrames5->pushBack(spritecache->getSpriteFrameByName("fall1/fall4.png"));

	sprite = Sprite::createWithSpriteFrame(animFrames1->front());

	auto animationFly = Animation::createWithSpriteFrames(*animFrames1, 1.0f / 24);
	animationFly->setLoops(-1);
	auto actionFly = Animate::create(animationFly);
	sprite->runAction(actionFly);

	fallingSpeed = visibleSize.height * 0.00001;
	flyingSpeed = visibleSize.height * 0.00001;
	gravityAccel = visibleSize.height * 0.0000005;

	// ������ ���������� ����. ������ �������� - ��� ����(�� ������ � ������ �����), ������ �������� - ��������, � ������� �� ������ ���������� ������������ ��������� ��������.
	spriteBody = MyBodyParser::getInstance()->bodyFormJson(sprite, "obama", PhysicsMaterial(1.0f, 0.0f, 1.0f));
	spriteBody->setCollisionBitmask(PRESIDENT_FULL_BODY_COLLISION_BITMASK);
	spriteBody->setContactTestBitmask(true);

	auto bodyMouth = MyBodyParser::getInstance()->bodyFormJson(sprite, "obama_mouth", PhysicsMaterial(1.0f, 0.0f, 1.0f));
	bodyMouth->setCollisionBitmask(PRESIDENT_MOUTH_COLLISION_BITMASK);
	bodyMouth->setContactTestBitmask(true);

	auto bodySkull = MyBodyParser::getInstance()->bodyFormJson(sprite, "obama_skull", PhysicsMaterial(1.0f, 0.0f, 1.0f));
	bodySkull->setCollisionBitmask(PRESIDENT_SKULL_COLLISION_BITMASK);
	bodySkull->setContactTestBitmask(true);

	sprite->addComponent(spriteBody);

	isFalling = true;

	this->scheduleUpdate();

	this->addChild(sprite);

	sprite->setAnchorPoint(Vec2(0, 0.5));
	sprite->setPosition(Vec2(0, origin.y + visibleSize.height / 2));

	nodeMouth = Node::create();
	nodeMouth->setAnchorPoint(Vec2(0, 0.5));
	nodeMouth->setPosition(Vec2(0, origin.y + visibleSize.height / 2));
	nodeMouth->setContentSize(sprite->getContentSize());
	nodeMouth->addComponent(bodyMouth);

	nodeSkull = Node::create();
	nodeSkull->setAnchorPoint(Vec2(0, 0.5));
	nodeSkull->setPosition(Vec2(0, origin.y + visibleSize.height / 2));
	nodeSkull->setContentSize(sprite->getContentSize());
	nodeSkull->addComponent(bodySkull);

	this->addChild(nodeMouth);
	this->addChild(nodeSkull);

	setName(NAME_PRESIDENT);

	gameOver = false;
	damaged = false;
}

void President::fly()
{
	isFalling = false;
	fallingSpeed = visibleSize.height * 0.00001;

	//spriteBody->setVelocity(Vec2(0, 0));
	//spriteBody->setAngularVelocity(0);
}

void President::eat()
{
	sprite->stopAllActions();

	auto animationFly = Animation::createWithSpriteFrames(*animFrames1, 1.0f / 24);
	animationFly->setLoops(-1);
	auto actionFly = Animate::create(animationFly);

	auto animationEat = Animation::createWithSpriteFrames(*animFrames2, 1.0f / 24);
	auto actionEat = Animate::create(animationEat);
	auto sequence = Sequence::create(actionEat, actionFly, nullptr);
	sprite->runAction(sequence);
}

void President::fall()
{
	isFalling = true;
}

void President::update(float dt)
{
	if (isFalling)
	{
		//fallingSpeed = fallingSpeed + gravityAccel;

		sprite->setPositionY(sprite->getPositionY() - (fallingSpeed * visibleSize.height));
		nodeMouth->setPositionY(nodeMouth->getPositionY() - (fallingSpeed * visibleSize.height));
		nodeSkull->setPositionY(nodeSkull->getPositionY() - (fallingSpeed * visibleSize.height));
	}
	else
	{
		sprite->setPositionY(sprite->getPositionY() + (flyingSpeed * visibleSize.height));
		nodeMouth->setPositionY(nodeMouth->getPositionY() + (flyingSpeed * visibleSize.height));
		nodeSkull->setPositionY(nodeMouth->getPositionY() + (flyingSpeed * visibleSize.height));
	}

	if (sprite->getPositionY() + 300 < 0)
	{
		gameOver = true;
	}
}

void President::damaging()
{
	damaged = true;

	sprite->stopAllActions();

	auto animationDamaging = Animation::createWithSpriteFrames(*animFrames4, 1.0f / 24);
	animationDamaging->setLoops(-1);
	auto actionDamaging = Animate::create(animationDamaging);
	sprite->runAction(actionDamaging);
}

void President::burn()
{
	damaged = true;

	sprite->stopAllActions();

	auto animationBurning = Animation::createWithSpriteFrames(*animFrames3, 1.0f / 24);
	animationBurning->setLoops(5);
	auto actionBurning = Animate::create(animationBurning);

	auto animationBurned = Animation::createWithSpriteFrames(*animFrames4, 1.0f / 24);
	animationBurned->setLoops(-1);
	auto actionBurned = Animate::create(animationBurned);
	auto sequence = Sequence::create(actionBurning, actionBurned, nullptr);
	sprite->runAction(sequence);
}

bool President::isDamaged()
{
	return damaged;
}

bool President::isGameOver()
{
	return gameOver;
}

President::~President()
{

}

