#pragma once

#include "cocos2d.h"

USING_NS_CC;

const std::string NAME_PRESIDENT = "president";

class President : public cocos2d::Node
{
public:
	President();
	void fly();
	void fall();
	void burn();
	void damaging();
	bool isGameOver();
	bool isDamaged();
	void eat();
	~President();
private:
	virtual void update(float dt);

	Vector<SpriteFrame*>* animFrames1;
	Vector<SpriteFrame*>* animFrames2;
	Vector<SpriteFrame*>* animFrames3;
	Vector<SpriteFrame*>* animFrames4;
	Vector<SpriteFrame*>* animFrames5;

	Size visibleSize;
	Vec2 origin;
	bool gameOver;
	bool damaged;
	bool isFalling;
	
	float fallingSpeed;
	float flyingSpeed;
	float gravityAccel;

	Sprite* sprite;
	Node* nodeMouth;
	Node* nodeSkull;

	PhysicsBody* spriteBody;
};

