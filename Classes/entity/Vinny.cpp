#include "Vinny.h"

#include "CollisionBitmasks.h"

Vinny::Vinny()
{
	auto spritecache = SpriteFrameCache::getInstance();

	int typeCloud = RandomHelper::random_int(1, 2);

	std::string bodyName = "vinny";

	animFrames = new Vector<SpriteFrame*>();
	animFrames->pushBack(spritecache->getSpriteFrameByName("vinny1.png"));
	animFrames->pushBack(spritecache->getSpriteFrameByName("vinny2.png"));

	sprite = Sprite::createWithSpriteFrame(animFrames->front());

	auto animation = Animation::createWithSpriteFrames(*animFrames, 1.0f / 24);
	auto animate = Animate::create(animation);
	sprite->runAction(RepeatForever::create(animate));

	spriteBody = MyBodyParser::getInstance()->bodyFormJson(sprite, bodyName, PhysicsMaterial(1.0f, 0.0f, 1.0f));
	spriteBody->setCollisionBitmask(VINNY_COLLISION_BITMASK);
	spriteBody->setContactTestBitmask(true);

	sprite->setPhysicsBody(spriteBody);
	this->addChild(sprite);


	startRun();

	setName(NAME_VINNY);
}

void Vinny::startRun()
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	moveSpeed = 0.01;
	auto moveAction = MoveTo::create(moveSpeed * visibleSize.width, Point(sprite->getPositionX(), origin.y + visibleSize.height + 300));
	sprite->runAction(moveAction);
}

void Vinny::refresh()
{
	removed = false;

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	int posVinny = RandomHelper::random_int((int)(origin.x + sprite->getContentSize().width), (int)(origin.x + visibleSize.width - sprite->getContentSize().width));
	sprite->setAnchorPoint(Vec2(0, 1));
	sprite->setPosition(Vec2(origin.x + posVinny, 0));
	spriteBody->setOwner(sprite);
	sprite->setParent(this);
	MovingEntity::startRun();
	startRun();

	auto animation = Animation::createWithSpriteFrames(*animFrames, 1.0f / 24);
	auto animate = Animate::create(animation);
	sprite->runAction(RepeatForever::create(animate));
}

Vinny::~Vinny()
{
}
