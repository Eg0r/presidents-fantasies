#include "Star.h"

#include "CollisionBitmasks.h"

//Vector<Star*>* Star::stars = new Vector<Star*>();

bool Star::init()
{
	if (!Node::init())
	{
		return false;
	}

	auto spritecache = SpriteFrameCache::getInstance();

	int typeCloud = RandomHelper::random_int(1, 2);

	std::string bodyName = "star";

	animFrames = new Vector<SpriteFrame*>();
	animFrames->pushBack(spritecache->getSpriteFrameByName("star1.png"));
	animFrames->pushBack(spritecache->getSpriteFrameByName("star2.png"));
	animFrames->pushBack(spritecache->getSpriteFrameByName("star3.png"));
	animFrames->pushBack(spritecache->getSpriteFrameByName("star4.png"));

	sprite = Sprite::createWithSpriteFrame(animFrames->front());

	spriteBody = MyBodyParser::getInstance()->bodyFormJson(sprite, bodyName, PhysicsMaterial(1.0f, 0.0f, 1.0f));
	spriteBody->setCollisionBitmask(STAR_COLLISION_BITMASK);
	spriteBody->setContactTestBitmask(true);

	sprite->setPhysicsBody(spriteBody);
	this->addChild(sprite);

	setName(NAME_STAR);

	return true;
}

Star::Star()
{
	
}

void Star::setPosition(const Vec2& position)
{
	sprite->setAnchorPoint(Vec2(0.5, 0.5));
	sprite->setPosition(position);
}

void Star::refresh()
{
	removed = false;

	auto animation = Animation::createWithSpriteFrames(*animFrames, 1.0f / 24);
	auto animate = Animate::create(animation);
	sprite->runAction(RepeatForever::create(animate));

	spriteBody->setOwner(sprite);
	sprite->setParent(this);

	Size visibleSize = Director::getInstance()->getVisibleSize();
	moveSpeed = 0.001;
	auto moveAction = MoveTo::create(moveSpeed * visibleSize.width, Point(-400, sprite->getPositionY()));
	sprite->runAction(moveAction);
}

Star::~Star()
{
}
