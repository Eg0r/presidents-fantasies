#pragma once

#include "MovingEntity.h"

class Star: public MovingEntity<Star>
{
public:
	Star();
	~Star();

	virtual bool init();
	
	void setPosition(const Vec2&) override;
	virtual void refresh();
private:
	//static Vector<Star*>* stars;

	Vector<SpriteFrame*>* animFrames;

};

