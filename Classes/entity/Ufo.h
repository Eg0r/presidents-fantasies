#pragma once

#include "MovingEntity.h"

class Ufo: public MovingEntity<Ufo>
{
public:
	Ufo();
	~Ufo();

	virtual bool init();

	virtual void refresh();
	void fall();
	const Vec2& getPosition() const override;
	
private:
	//static Vector<Ufo*>* ufos;

	void update(float dt);

	Vector<SpriteFrame*>* animFrames;
	Node* nodeVuln;
	bool falling;
};

