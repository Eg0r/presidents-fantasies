#pragma once

#include "MovingEntity.h"

enum GromType
{
	FirstGromType = 0,
	GromType1 = 0,
	GromType2 = 1,
	GromType3 = 2,
	LastGromType = 2
};

class Grom: public MovingEntity<Grom>
{
public:
	Grom();
	~Grom();

	virtual void refresh();
	bool isGromEnable();
private:
	void updateTimer(float dt);

	Vector<SpriteFrame*>* animFrames1;
	Vector<SpriteFrame*>* animFrames2;
	Vector<SpriteFrame*>* animFrames3;

	bool gromEnable;
};

