#pragma once

#include "MovingEntity.h"

class Vinny: public MovingEntity<Vinny>
{
public:
	Vinny();
	~Vinny();
	virtual void refresh();
	virtual void startRun();
private:
	Vector<SpriteFrame*>* animFrames;
};

