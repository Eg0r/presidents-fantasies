#include "Cloud.h"

#include "CollisionBitmasks.h"

//Vector<Cloud*>* Cloud::clouds = new Vector<Cloud*>();

bool Cloud::init()
{
	if (!Node::init())
	{
		return false;
	}

	auto spritecache = SpriteFrameCache::getInstance();

	int typeCloud = RandomHelper::random_int(1, 2);

	std::string spriteName = "cloud" + std::to_string(typeCloud) + ".png";
	std::string bodyName = "cloud" + std::to_string(typeCloud);

	sprite = Sprite::createWithSpriteFrameName(spriteName);
	spriteBody = MyBodyParser::getInstance()->bodyFormJson(sprite, bodyName, PhysicsMaterial(1.0f, 0.0f, 1.0f));
	spriteBody->setCollisionBitmask(CLOUD_COLLISION_BITMASK);
	spriteBody->setContactTestBitmask(true);

	sprite->setPhysicsBody(spriteBody);
	this->addChild(sprite);

	setName(NAME_CLOUD);

	return true;
}

Cloud::Cloud()
{
	
}

Cloud::~Cloud()
{
}
