#pragma once

#include "cocos2d.h"

#include "MyBodyParser.h"

USING_NS_CC;

const std::string NAME_CLOUD = "cloud";
const std::string NAME_VINNY = "vinny";
const std::string NAME_UFO = "ufo";
const std::string NAME_GROM = "grom";
const std::string NAME_STAR = "star";

template <typename T>
class MovingEntity: public Node
{
public:
	MovingEntity();
	~MovingEntity();

	static T* create();
	static void pool(T* entity);

	virtual void startRun();
	virtual void refresh();

	void remove();
	bool isRemoved();
	bool isOutLeftBound();
protected:
	static Vector<T*>* entities;

	PhysicsBody* spriteBody;
	Sprite* sprite;
	bool removed;
	float moveSpeed;
private:
};

template <typename T>
Vector<T*>* MovingEntity<T>::entities = new Vector<T*>();

template <typename T>
T* MovingEntity<T>::create()
{
	T* ret;
	if (T::entities->size() > 0)
	{
		ret = T::entities->back();
		T::entities->popBack();
	}
	else
	{
		ret = new T();
		if (ret && ret->init())
		{
			//ret->autorelease();
		}
		else
		{
			CC_SAFE_DELETE(ret);
		}
	}
	return ret;
}

template <typename T>
MovingEntity<T>::MovingEntity()
{
	removed = false;
}

template <typename T>
void MovingEntity<T>::remove()
{
	removed = true;
	sprite->stopAllActions();
}

template <typename T>
bool MovingEntity<T>::isRemoved()
{
	return removed;
}

template <typename T>
void MovingEntity<T>::startRun()
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	moveSpeed = 0.01;
	auto moveAction = MoveTo::create(moveSpeed * visibleSize.width, Point(-300, sprite->getPositionY()));
	sprite->runAction(moveAction);
}

template <typename T>
void MovingEntity<T>::refresh()
{
	removed = false;

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	int posCloud = RandomHelper::random_int((int)origin.y, (int)(origin.y + visibleSize.height - sprite->getContentSize().height));
	sprite->setAnchorPoint(Vec2(0, 0));
	sprite->setPosition(Vec2(origin.x + visibleSize.width, posCloud));
	spriteBody->setOwner(sprite);
	sprite->setParent(this);
	startRun();
}

template <typename T>
void MovingEntity<T>::pool(T* entity)
{
	entities->pushBack(entity);
}

template <typename T>
bool MovingEntity<T>::isOutLeftBound()
{
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	if (sprite->getPositionX() < origin.x - sprite->getContentSize().width)
	{
		return true;
	}
	return false;
}

template <typename T>
MovingEntity<T>::~MovingEntity()
{
}
