#include "MovingEntityFactory.h"



MovingEntityFactory::MovingEntityFactory()
{

}

Node* MovingEntityFactory::factoryMethod()
{
	int type = RandomHelper::random_int((int)FIRST, (int)LAST);
	switch (type)
	{
		case NOT: 
			return nullptr; 
		break;
		case CLOUD:
			Cloud* cloud;
			cloud = Cloud::create();
			cloud->refresh();
			return cloud;
		break;
		case UFO:
			Ufo* ufo;
			ufo = Ufo::create();
			ufo->refresh();
			return ufo;
		break;
		case VINNY:
			Vinny* vinny;
			vinny = Vinny::create();
			vinny->refresh();
			return vinny;
		break;
		case GROM:
			Grom* grom;
			grom = Grom::create();
			grom->refresh();
			return grom;
		break;
	}
}

MovingEntityFactory::~MovingEntityFactory()
{
}
