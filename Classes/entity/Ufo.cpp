#include "Ufo.h"

#include "CollisionBitmasks.h"

bool Ufo::init()
{
	if (!Node::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	auto origin = Director::getInstance()->getVisibleOrigin();

	auto spritecache = SpriteFrameCache::getInstance();

	int typeCloud = RandomHelper::random_int(1, 2);

	std::string bodyName = "ufo";

	animFrames = new Vector<SpriteFrame*>();
	animFrames->pushBack(spritecache->getSpriteFrameByName("ufo1.png"));
	animFrames->pushBack(spritecache->getSpriteFrameByName("ufo2.png"));
	animFrames->pushBack(spritecache->getSpriteFrameByName("ufo3.png"));
	animFrames->pushBack(spritecache->getSpriteFrameByName("ufo4.png"));

	sprite = Sprite::createWithSpriteFrame(animFrames->front());

	auto animation = Animation::createWithSpriteFrames(*animFrames, 1.0f / 24);
	auto animate = Animate::create(animation);
	sprite->runAction(RepeatForever::create(animate));

	spriteBody = MyBodyParser::getInstance()->bodyFormJson(sprite, bodyName, PhysicsMaterial(1.0f, 0.0f, 1.0f));
	spriteBody->setCollisionBitmask(UFO_COLLISION_BITMASK);
	spriteBody->setContactTestBitmask(true);



	sprite->setPhysicsBody(spriteBody);
	this->addChild(sprite);

	auto vulBody = MyBodyParser::getInstance()->bodyFormJson(sprite, "ufo_vuln", PhysicsMaterial(1.0f, 0.0f, 1.0f));
	vulBody->setCollisionBitmask(UFO_VULN_COLLISION_BITMASK);
	vulBody->setContactTestBitmask(true);

	nodeVuln = Node::create();
	nodeVuln->setContentSize(sprite->getContentSize());
	nodeVuln->addComponent(vulBody);

	this->addChild(nodeVuln);


	startRun();

	setName(NAME_UFO);

	return true;
}

Ufo::Ufo()
{
	
}

void Ufo::update(float dt)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	float newPosX = sprite->getPositionX() - (moveSpeed * visibleSize.width);
	sprite->setPositionX(newPosX);
	nodeVuln->setPositionX(newPosX);

	if (falling)
	{
		float newPosY = sprite->getPositionY() - (0.02 * visibleSize.width);
		sprite->setPositionY(newPosY);
		nodeVuln->setPositionY(newPosY);
	}
}

const Vec2& Ufo::getPosition() const
{
	return sprite->getPosition();
}

void Ufo::refresh()
{
	removed = false;
	falling = false;

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	int posY = RandomHelper::random_int((int)(origin.y + sprite->getContentSize().height), (int)(origin.y + visibleSize.height - sprite->getContentSize().height));
	sprite->setAnchorPoint(Vec2(0.5, 0.5));
	sprite->setPosition(Vec2(origin.x + visibleSize.width + sprite->getContentSize().width, posY));
	nodeVuln->setAnchorPoint(Vec2(0.5, 0.5));
	nodeVuln->setPosition(Vec2(origin.x + visibleSize.width + sprite->getContentSize().width, posY));
	spriteBody->setOwner(sprite);
	sprite->setParent(this);

	moveSpeed = 0.001;

	auto animation = Animation::createWithSpriteFrames(*animFrames, 1.0f / 24);
	auto animate = Animate::create(animation);
	sprite->runAction(RepeatForever::create(animate));

	scheduleUpdate();
}

void Ufo::fall()
{
	falling = true;
}

Ufo::~Ufo()
{
}
