#include "Grom.h"

#include "CollisionBitmasks.h"

Grom::Grom()
{
	auto spritecache = SpriteFrameCache::getInstance();

	int typeCloud = RandomHelper::random_int(1, 2);

	std::string bodyName = "grom";

	animFrames1 = new Vector<SpriteFrame*>();
	animFrames1->pushBack(spritecache->getSpriteFrameByName("grom1/grom1.png"));
	animFrames1->pushBack(spritecache->getSpriteFrameByName("grom1/grom2.png"));
	animFrames1->pushBack(spritecache->getSpriteFrameByName("grom1/grom3.png"));
	animFrames1->pushBack(spritecache->getSpriteFrameByName("grom1/grom4.png"));

	animFrames2 = new Vector<SpriteFrame*>();
	animFrames2->pushBack(spritecache->getSpriteFrameByName("grom2/grom1.png"));
	animFrames2->pushBack(spritecache->getSpriteFrameByName("grom2/grom2.png"));
	animFrames2->pushBack(spritecache->getSpriteFrameByName("grom2/grom3.png"));
	animFrames2->pushBack(spritecache->getSpriteFrameByName("grom2/grom4.png"));

	animFrames3 = new Vector<SpriteFrame*>();
	animFrames3->pushBack(spritecache->getSpriteFrameByName("grom3/grom1.png"));
	animFrames3->pushBack(spritecache->getSpriteFrameByName("grom3/grom2.png"));
	animFrames3->pushBack(spritecache->getSpriteFrameByName("grom3/grom3.png"));
	animFrames3->pushBack(spritecache->getSpriteFrameByName("grom3/grom4.png"));

	sprite = Sprite::createWithSpriteFrameName("grom0.png");

	/*auto animation = Animation::createWithSpriteFrames(animFrames, 1.0f / 24);
	auto animate = Animate::create(animation);
	sprite->runAction(RepeatForever::create(animate));*/

	spriteBody = MyBodyParser::getInstance()->bodyFormJson(sprite, bodyName, PhysicsMaterial(1.0f, 0.0f, 1.0f));
	spriteBody->setCollisionBitmask(GROM_COLLISION_BITMASK);
	spriteBody->setContactTestBitmask(true);

	sprite->setPhysicsBody(spriteBody);
	this->addChild(sprite);


	startRun();

	setName(NAME_GROM);
}

void Grom::updateTimer(float dt)
{
	Vector<SpriteFrame*>* frames;
	int gromType = RandomHelper::random_int((int)FirstGromType, (int)LastGromType);
	switch (gromType)
	{
		case GromType1:
			frames = animFrames1;
		break;
		case GromType2:
			frames = animFrames2;
		break;
		case GromType3:
			frames = animFrames3;
		break;
	}

	gromEnable = true;

	auto animation = Animation::createWithSpriteFrames(*frames, 1.0f / 24);
	animation->setLoops(3);
	auto actionHit = Animate::create(animation);
	auto actionIdle = CallFunc::create([this]() 
	{ 
		sprite->setSpriteFrame("grom0.png"); 
		gromEnable = false;
	});
	Sequence* sequence = Sequence::create(actionHit, actionIdle, nullptr);
	sprite->runAction(sequence);
}

bool Grom::isGromEnable()
{
	return gromEnable;
}

void Grom::refresh()
{
	MovingEntity::refresh();
	sprite->setSpriteFrame("grom0.png");
	schedule(schedule_selector(Grom::updateTimer), 4.f);
	gromEnable = false;
}

Grom::~Grom()
{
}
