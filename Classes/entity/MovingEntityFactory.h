#pragma once

#include "MovingEntity.h"
#include "Cloud.h"
#include "Ufo.h"
#include "Vinny.h"
#include "Grom.h"
#include "Star.h"

enum TypeEntity
{
	FIRST = 0,
	NOT = 0,
	CLOUD = 1,
	UFO = 2,
	VINNY = 3,
	GROM = 4,
	LAST = 4
};

class MovingEntityFactory
{
public:
	MovingEntityFactory();
	Node* factoryMethod();
	~MovingEntityFactory();
};

