#pragma once

#include "cocos2d.h"

USING_NS_CC;

class ProgressInfo: public Node
{
public:
	static ProgressInfo* create();

	virtual bool init();
	void incCloud();
	void incStar();
private:
	Label* cloudLabel;
	Label* starLabel;

	int cloudCounter;
	int starCounter;

	ProgressInfo();
	~ProgressInfo();
};

