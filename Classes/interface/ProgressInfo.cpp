#include "ProgressInfo.h"


ProgressInfo* ProgressInfo::create()
{
	ProgressInfo* ret = new ProgressInfo();
	if (ret && ret->init())
	{
		ret->autorelease();
	}
	else
	{
		CC_SAFE_DELETE(ret);
	}
	return ret;
}

bool ProgressInfo::init()
{
	if (!Node::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto cloudIcon = Sprite::create("./interface/cloud icon.png");
	cloudIcon->setAnchorPoint(Vec2(0, 1));
	cloudIcon->setPosition(Vec2(origin.x, origin.y + visibleSize.height));
	this->addChild(cloudIcon);

	auto starIcon = Sprite::create("./interface/star icon.png");
	starIcon->setAnchorPoint(Vec2(0, 1));
	starIcon->setPosition(Vec2(origin.x + cloudIcon->getContentSize().width, origin.y + visibleSize.height));
	this->addChild(starIcon);

	cloudCounter = 0;
	starCounter = 0;

	cloudLabel = Label::create("0", "fonts/trashco.ttf", 55);
	cloudLabel->setAnchorPoint(Vec2(0, 1));
	cloudLabel->setPosition(Vec2(origin.x + cloudIcon->getContentSize().width * 1.13, origin.y + visibleSize.height * 0.965));
	this->addChild(cloudLabel);

	starLabel = Label::create("0", "fonts/trashco.ttf", 55);
	starLabel->setAnchorPoint(Vec2(0, 1));
	starLabel->setPosition(Vec2(starIcon->getPositionX() + starIcon->getContentSize().width * 1.13, origin.y + visibleSize.height * 0.965));
	this->addChild(starLabel);

	return true;
}

void ProgressInfo::incCloud()
{
	cloudCounter++;
	cloudLabel->setString(std::to_string(cloudCounter));
}

void ProgressInfo::incStar()
{
	starCounter++;
	starLabel->setString(std::to_string(starCounter));
}

ProgressInfo::ProgressInfo()
{

}

ProgressInfo::~ProgressInfo()
{
}
