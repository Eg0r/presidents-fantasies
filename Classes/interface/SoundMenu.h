#pragma once

#include "cocos2d.h"

USING_NS_CC;

enum SoundPosition
{
	Top,
	NoTop
};

class SoundMenu: public cocos2d::Node
{
public:
	SoundMenu(SoundPosition soundPosition);
	~SoundMenu();
private:
	void menuSoundEnableCallback(Ref* pSender);
	void menuSoundDisableCallback(Ref* pSender);

	MenuItemImage* soundEnableItem;
	MenuItemImage* soundDisableItem;
};

