#include "SoundMenu.h"



SoundMenu::SoundMenu(SoundPosition soundPosition)
{
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	soundEnableItem = MenuItemImage::create("./interface/sound.png", "./interface/sound.png", CC_CALLBACK_1(SoundMenu::menuSoundEnableCallback, this));


	int pos = 0;
	switch (soundPosition)
	{
	case Top:
		pos = 0;
		break;
	case NoTop:
		pos = soundEnableItem->getContentSize().height;
		break;
	}

	soundEnableItem->setAnchorPoint(Vec2(1, 1));
	soundEnableItem->setPosition(Vec2(origin.x + visibleSize.width, origin.y + visibleSize.height - pos));

	soundDisableItem = MenuItemImage::create("./interface/not sound.png", "./interface/not sound.png", CC_CALLBACK_1(SoundMenu::menuSoundDisableCallback, this));
	soundDisableItem->setAnchorPoint(Vec2(1, 1));
	soundDisableItem->setPosition(Vec2(origin.x + visibleSize.width, origin.y + visibleSize.height - pos));

	soundEnableItem->setVisible(true);
	soundDisableItem->setVisible(false);

	auto menu = Menu::create(soundEnableItem, soundDisableItem, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, 1);
}

SoundMenu::~SoundMenu()
{
}

void SoundMenu::menuSoundEnableCallback(Ref* pSender)
{
	soundEnableItem->setVisible(false);
	soundDisableItem->setVisible(true);
}

void SoundMenu::menuSoundDisableCallback(Ref* pSender)
{
	soundEnableItem->setVisible(true);
	soundDisableItem->setVisible(false);
}
