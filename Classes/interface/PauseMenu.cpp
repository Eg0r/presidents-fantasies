#include "PauseMenu.h"


PauseMenu* PauseMenu::create()
{
	PauseMenu* ret = new PauseMenu();
	if (ret && ret->init())
	{
		ret->autorelease();
	}
	else
	{
		CC_SAFE_DELETE(ret);
	}
	return ret;
}

bool PauseMenu::init()
{
	if (!Node::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	pauseItem = MenuItemImage::create("./interface/pause.png", "./interface/pause.png", CC_CALLBACK_1(PauseMenu::menuPauseEnableCallback, this));
	pauseItem->setAnchorPoint(Vec2(1, 1));
	pauseItem->setPosition(Vec2(origin.x + visibleSize.width, origin.y + visibleSize.height));

	playItem = MenuItemImage::create("./interface/play.png", "./interface/play.png", CC_CALLBACK_1(PauseMenu::menuPlayEnableCallback, this));
	playItem->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2));
	playItem->setVisible(false);

	auto menu = Menu::create(pauseItem, playItem, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, 1);

	return true;
}

void PauseMenu::menuPauseEnableCallback(Ref* pSender)
{
	Director::getInstance()->pause();
	playItem->setVisible(true);
}

void PauseMenu::menuPlayEnableCallback(Ref* pSender)
{
	Director::getInstance()->resume();
	playItem->setVisible(false);
}

PauseMenu::PauseMenu()
{
}


PauseMenu::~PauseMenu()
{
}
