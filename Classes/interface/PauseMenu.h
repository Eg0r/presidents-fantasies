#pragma once

#include "cocos2d.h"

USING_NS_CC;

class PauseMenu: public cocos2d::Node
{
public:
	static PauseMenu* create();

	virtual bool init();
private:
	PauseMenu();
	void menuPauseEnableCallback(Ref* pSender);
	void menuPlayEnableCallback(Ref* pSender);
	~PauseMenu();

	MenuItemImage* pauseItem;
	MenuItemImage* playItem;
};

