#include "GameScene.h"

#include "CollisionBitmasks.h"

USING_NS_CC;


Scene* GameScene::createScene()
{
	auto scene = Scene::createWithPhysics();
	scene->getPhysicsWorld()->setGravity(Vec2(0, 0));
	scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);

	auto layer = GameScene::create();
	scene->addChild(layer);

	return scene;
}

bool GameScene::init()
{
	if (!Scene::init())
	{
		return false;
	}

	//0xFFc6e7f0
	Color4B background = Color4B(0xC6, 0xE7, 0xF0, 255);
	auto bg = cocos2d::LayerColor::create(background);
	this->addChild(bg);

	president = new President();
	this->addChild(president, 1);

	SoundMenu* soundMenu = new SoundMenu(SoundPosition::NoTop);
	this->addChild(soundMenu, 1);

	auto listener = EventListenerMouse::create();
	listener->onMouseDown = CC_CALLBACK_1(GameScene::onMouseDown, this);
	listener->onMouseUp = CC_CALLBACK_1(GameScene::onMouseUp, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	scheduleUpdate();
	schedule(schedule_selector(GameScene::updateTimer), 3.f);

	movingEntityFactory = new MovingEntityFactory();

	PauseMenu* pauseMenu = PauseMenu::create();
	this->addChild(pauseMenu, 2);
	
	progressInfo = ProgressInfo::create();
	this->addChild(progressInfo, 3);
	
	auto contactListener = EventListenerPhysicsContact::create();
	contactListener->onContactBegin = CC_CALLBACK_1(GameScene::onContactBegin, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(contactListener, this);

	//starCreate = false;

	return true;
}

bool GameScene::onContactBegin(PhysicsContact &contact)
{
	PhysicsBody *a = contact.getShapeA()->getBody();
	PhysicsBody *b = contact.getShapeB()->getBody();
	int aBitMask = a->getCollisionBitmask();
	int bBitMask = b->getCollisionBitmask();
		
	if ((aBitMask == PRESIDENT_MOUTH_COLLISION_BITMASK) && (bBitMask == CLOUD_COLLISION_BITMASK) && (!president->isDamaged()))
	{
		Node* node = b->getOwner();
		if (node != nullptr)
		{
			Cloud* cloud = (Cloud*)(node->getParent());
			if (!cloud->isRemoved())
			{
				Cloud::pool(cloud);
				cloud->remove();
				this->removeChild(cloud);
				cloud->setParent(nullptr);
				president->eat();
				progressInfo->incCloud();
			}
		}
	}
	else if ((aBitMask == CLOUD_COLLISION_BITMASK && bBitMask == PRESIDENT_MOUTH_COLLISION_BITMASK) && (!president->isDamaged()))
	{
		Node* node = a->getOwner();
		if (node != nullptr)
		{
			Cloud* cloud = (Cloud*)(node->getParent());
			if (!cloud->isRemoved())
			{
				Cloud::pool(cloud);
				cloud->remove();
				this->removeChild(cloud);
				cloud->setParent(nullptr);
				president->eat();
				progressInfo->incCloud();
			}
		}
		//sceneWorld->removeBody(a);
	}

	else if ((aBitMask == GROM_COLLISION_BITMASK && bBitMask == PRESIDENT_FULL_BODY_COLLISION_BITMASK) && (!president->isDamaged()))
	{
		Node* node = a->getOwner();
		if (node != nullptr)
		{
			Grom* grom = (Grom*)(node->getParent());
			if (grom->isGromEnable())
			{
				president->burn();
			}
		}
	}
	else if ((aBitMask == PRESIDENT_FULL_BODY_COLLISION_BITMASK && bBitMask == GROM_COLLISION_BITMASK) && (!president->isDamaged()))
	{
		Node* node = b->getOwner();
		if (node != nullptr)
		{
			Grom* grom = (Grom*)(node->getParent());
			if (grom->isGromEnable())
			{
				president->burn();
			}
		}
	}
	else if ((aBitMask == PRESIDENT_SKULL_COLLISION_BITMASK && bBitMask == UFO_VULN_COLLISION_BITMASK) && (!president->isDamaged()))
	{
		Node* node = b->getOwner();
		if (node != nullptr)
		{
			Ufo* ufo = (Ufo*)(node->getParent());
			const Vec2& position = ufo->getPosition();
			starCoord = &position;
			ufo->fall();
			starCreate = true;
		}
	}
	else if ((aBitMask == UFO_VULN_COLLISION_BITMASK && bBitMask == PRESIDENT_SKULL_COLLISION_BITMASK) && (!president->isDamaged()))
	{
		Node* node = a->getOwner();
		if (node != nullptr)
		{
			Ufo* ufo = (Ufo*)(node->getParent());
			const Vec2& position = ufo->getPosition();
			starCoord = &position;
			ufo->fall();
			starCreate = true;
		}
	}

	return false;
}

void GameScene::update(float dt)
{
	for (int i = 0; i < getChildrenCount(); i++)
	{
		Node* child = getChildren().at(i);
		std::string name = child->getName();
		if (name == NAME_CLOUD)
		{
			Cloud* cloud = (Cloud*)child;
			if (cloud->isOutLeftBound())
			{
				Cloud::pool(cloud);
				cloud->remove();
				this->removeChild(cloud);
				cloud->setParent(nullptr);
				i--;
			}
		}
		else if (name == NAME_UFO)
		{
			Ufo* ufo = (Ufo*)child;
			if (ufo->isOutLeftBound())
			{
				Ufo::pool(ufo);
				ufo->remove();
				this->removeChild(ufo);
				ufo->setParent(nullptr);
				i--;
			}
		}
		else if (name == NAME_VINNY)
		{
			Vinny* vinny = (Vinny*)child;
			if (vinny->isOutLeftBound())
			{
				Vinny::pool(vinny);
				vinny->remove();
				this->removeChild(vinny);
				vinny->setParent(nullptr);
				i--;
			}
		}
		else if (name == NAME_GROM)
		{
			Grom* grom = (Grom*)child;
			if (grom->isOutLeftBound())
			{
				Grom::pool(grom);
				grom->remove();
				this->removeChild(grom);
				grom->setParent(nullptr);
				i--;
			}
		}
		else if (name == NAME_STAR)
		{
			Star* star = (Star*)child;
			if (star->isOutLeftBound())
			{
				Star::pool(star);
				star->remove();
				this->removeChild(star);
				star->setParent(nullptr);
				i--;
			}
		}
		else if (name == NAME_PRESIDENT)
		{
			if (president->isGameOver())
			{
				removeAll();
			}
		}
	}

	if (starCreate)
	{
		starCreate = false;

		Star* star = Star::create();
		star->refresh();
		star->setPosition(*starCoord);
		this->addChild(star);
	}
}

void GameScene::removeAll()
{

}

void GameScene::updateTimer(float dt)
{
	Node* entity = movingEntityFactory->factoryMethod();
	if (entity != nullptr)
	{
		this->addChild(entity);
	}
}

void GameScene::onMouseDown(EventMouse* event)
{
	president->fly();
}

void GameScene::onMouseUp(EventMouse* event)
{
	president->fall();
}

void GameScene::setPhysicsWorld(cocos2d::PhysicsWorld *world)
{ 
	sceneWorld = world; 
};